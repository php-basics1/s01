<?php require_once './code.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Activity</title>
</head>
<body>

    <h1>Full Address</h1>
        <p><?php echo $getFullAddress('2F Pascual Bldg.', 'Parañaque', 'Metro Manila', 'Philippines'); ?></p>
        <p><?php echo $getFullAddress('9F Rodriguez Bldg.', 'Marikina', 'Metro Manila', 'Philippines'); ?></p>
    
    <hr>

    <h1>Letter-Based Grading</h1>
        <p><?php echo letterGradingSystem(87); ?></p>
        <p><?php echo letterGradingSystem(94); ?></p>
        <p><?php echo letterGradingSystem(74); ?></p>
    <hr>
    
</body>
</html>