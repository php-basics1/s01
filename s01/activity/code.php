<!-- 1. Create code.php and index.php files inside the activity folder.
2. In code.php, create a function named getFullAddress() that will take four arguments:
    - Country
    - City
    - Province
    - Specific Address (such as block, lot, or building name and room number).
3. Have this function return the concatenated arguments to result in a coherent address.
4. Create another function named getLetterGrade() that uses conditional statements to output a letter representation of a given numerical grade:
    - A+ (98 to 100)
    - A (95 to 97)
    - A- (92 to 94)
    - B+ (89 to 91)
    - B (86 to 88)
    - B- (83 to 85)
    - C+ (80 to 82)
    - C (77 to 79)
    - C- (75 to 76)
    - F (75 below)
5. Include the code.php in the index.html and invoke the created methods. -->


<?php 
    // <!-- 1. Create code.php and index.php files inside the activity folder.
    // 2. In code.php, create a function named getFullAddress() that will take four arguments:
    //     - Country
    //     - City
    //     - Province
    //     - Specific Address (such as block, lot, or building name and room number).
    // 3. Have this function return the concatenated arguments to result in a coherent address.

    // MY SOLUTION:
    $getFullAddress = fn ($specificAddress, $city, $province, $country) => "$specificAddress, $city, $province, $country.";

    // 4. Create another function named getLetterGrade() that uses conditional statements to output a letter representation of a given numerical grade:
    //     - A+ (98 to 100)
    //     - A (95 to 97)
    //     - A- (92 to 94)
    //     - B+ (89 to 91)
    //     - B (86 to 88)
    //     - B- (83 to 85)
    //     - C+ (80 to 82)
    //     - C (77 to 79)
    //     - C- (75 to 76)
    //     - F (75 below)

    // MY SOLUTION:
    function letterGradingSystem($grade){
        if ($grade >= 98 && $grade <= 100) {
                return  $grade. ' is equivalent to A+';
            } elseif ($grade >= 95 && $grade <= 97) {
                return $grade. ' is equivalent to A';
            } elseif ($grade >= 92 && $grade <= 94) {
                return $grade. ' is equivalent to A-';
            } elseif ($grade >= 89 && $grade <= 91) {
                return $grade. ' is equivalent to B+';
            } elseif ($grade >= 86 && $grade <= 88) {
                return $grade. ' is equivalent to B';
            } elseif ($grade >= 83 && $grade <= 85) {
                return $grade. ' is equivalent to B-';
            } elseif ($grade >= 80 && $grade <= 82) {
                return $grade. ' is equivalent to C+';
            } elseif ($grade >= 77 && $grade <= 79) {
                return $grade. ' is equivalent to C';
            } elseif ($grade >= 75 && $grade <= 76) {
                return $grade. ' is equivalent to C-';
            } else {
                return $grade. ' is equivalent to F';
            }
    }

?>